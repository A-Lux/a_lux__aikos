import VueRouter from 'vue-router'
import Main from './pages/Main'
import Discount from './pages/Discount'
import QuestionAnswer from './pages/QuestionAnswer'
import Order from './pages/Order'
import Cart from './components/basket/Cart'
import Catalog from './pages/Catalog'
import SiteMap from './pages/SiteMap'
import TireService from './pages/TireService'
import CardProduct from './pages/CardProduct'
import Contact from './pages/Contact'
import SocialBar from './pages/SocialBar'
import DealerNetwork from './pages/DealerNetwork'
import TireModels from './pages/TireModels'






export default new VueRouter({
    routes: [{
            path: '/Main',
            component: Main
        },
        {
            path: '/Discount',
            component: Discount
        },
        {
            path: '/QuestionAnswer',
            component: QuestionAnswer
        },
        {
            path: '/Order',
            component: Order
        },
        {
            path: '/Cart',
            name: 'Cart',
            component: Cart,
            props: true
        },
        {
            path: '/Catalog',
            component: Catalog,
        },
        {
            path: '/SiteMap',
            component: SiteMap,
        },
        {
            path: '/TireService',
            component: TireService,
        },
        {
            path: '/CardProduct',
            component: CardProduct,
        },
        {
            path: '/Contact',
            component: Contact
        },
        {
            path: '/SocialBar',
            component: SocialBar
        },
        {
            path: '/DealerNetwork',
            component: DealerNetwork
        },
        {
            path: '/TireModels',
            component: TireModels
        }
    ]
})