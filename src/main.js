import Vue from 'vue'
import app from './App.vue'
import store from './vuex/store'
import vSelect from 'vue-select'
import axios from 'axios'
import VueRouter from 'vue-router'
import router from './routes'
import VueAgile from 'vue-agile'
import 'vueperslides/dist/vueperslides.css'

import 'vue-select/dist/vue-select.css';
import 'bootstrap'
import './plugin-css/bootstrap.css'
import '../node_modules/slick-carousel/slick/slick.css'
import '../node_modules/slick-carousel/slick/slick-theme.css'
import '../node_modules/vue-multiselect/dist/vue-multiselect.min.css'
import 'materialize-css/dist/js/materialize.min'

// IMPORT-ICON 

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faMinus } from '@fortawesome/free-solid-svg-icons'
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons'


Vue.use(VueAgile)


// IMPORT-ICON-END  


Vue.use(VueRouter)
Vue.config.productionTip = false
Vue.component('v-select', vSelect)
Vue.prototype.$http = axios;

new Vue({
    render: h => h(app),
    router,
    store
}).$mount('#app')


//  ICONS

library.add(faPlus)
library.add(faMinus)
library.add(faPlusSquare)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// ICONS-END